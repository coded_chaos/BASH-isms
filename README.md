# BASH-isms
```Shell
$home> for my BASH scripts
```

## ToDo List
- flesh out this [README](./README.md)
  - create ToC for all folders and scripts
  - provide help info for each scripts
- migrate scripts from github
- update 'work-in-progress' scripts

## Contact
<email-address>@<email-domain.dom>

#TEST
Here we are testing